"""
Point type definition
"""
from typing import Union, Annotated

Number = Union[int, float]
Point = Annotated[list[Number], 2]
