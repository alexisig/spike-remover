"""
Polygon Layer interface
"""

from typing import NamedTuple, Sequence

from models.polygon import Polygon


class PolygonLayer(NamedTuple):  # pylint: disable=missing-class-docstring
    name: str
    features: Sequence[Polygon]
    is_coordinate_system_geographic: bool
    properties: list[str]
    geometry: str
    crs: str
