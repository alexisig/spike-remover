"""
Polygon interface
"""
from typing import NamedTuple, Union

from models.point import Point
from utils import count_nested_lists


PolygonCoordinates = list[list[Point]]
MultiPolygonCoordinates = list[list[list[Point]]]


class Polygon(NamedTuple):  # pylint: disable=missing-class-docstring
    coordinates: Union[
        PolygonCoordinates,
        MultiPolygonCoordinates
    ]
    properties: dict

    def is_multipolygon(self) -> bool:
        """Check whether a polygon is a multipolygon by
        counting the amount of nested list it is made of

        Returns:
            bool: whether the check passed
        """
        return count_nested_lists(self.coordinates) == 3
