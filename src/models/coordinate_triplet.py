"""
CoordinateTriplet type definition
"""
from .point import Point

CoordinateTriplet = list[tuple[Point, Point, Point]]
