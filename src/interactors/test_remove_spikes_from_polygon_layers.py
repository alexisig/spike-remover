'''
Tests for the methods in src/interactors/remove_spikes_from_polygon_layers.py
'''

from unittest import TestCase

from models.coordinate_triplet import CoordinateTriplet
from models.polygon import Polygon

from .remove_spikes_from_polygon_layers import \
    get_angle_from_triplet, \
    is_distance_to_triplet_middle_point_above, \
    is_triplet_angle_above, \
    remove_spikes_from_polygon


# pylint: disable-next=missing-class-docstring
class TestGeopackageProcessor(TestCase):
    def test_get_angle_from_triplet(self):
        """
        Check that the methods calculate angles correctly. Both the angle
        values starting from 0 and 360 (to 0) are accepted as valid.
        """
        triplet_angle_pairs: list[tuple[CoordinateTriplet, int]] = [
            (
                [
                    (0, 0),
                    (1, 1),
                    (2, 2)
                ],
                180
            ),
            (
                [
                    (1, 0),
                    (0, 0),
                    (1, 1)
                ],
                45
            ),
            (
                [
                    (0, 0),
                    (0, 1),
                    (1, 1)
                ],
                90
            )
        ]
        for triplet, expected_angle in triplet_angle_pairs:
            processed_angle = get_angle_from_triplet(triplet)

            self.assertIn(
                processed_angle,
                [expected_angle, 360 - expected_angle],
                f"Angle should be {expected_angle}"
            )

    def test_is_distance_to_triplet_middle_point_above(self):
        """
        Check that the minimum distance check works properly
        by returning true if the distance to point a or point c
        to point b in a triplet is higher than the min_length
        value
        """
        # three closely located cities in france (less than 150km)
        lyon_coordinates = (45.764043, 4.835659)
        saint_etienne_coordinates = (45.439695, 4.3871779)
        grenoble_coordinates = (45.188529, 5.724524)

        triplet: CoordinateTriplet = [
            lyon_coordinates,
            saint_etienne_coordinates,
            grenoble_coordinates
        ]

        any_distance_to_saint_etienne_is_more_than_100 = \
            is_distance_to_triplet_middle_point_above(triplet, 100)
        any_distance_to_saint_etienne_is_more_than_1000 = \
            is_distance_to_triplet_middle_point_above(triplet, 1000)

        self.assertTrue(
            any_distance_to_saint_etienne_is_more_than_100,
            'Distance should be more than 100'
        )

        self.assertFalse(
            any_distance_to_saint_etienne_is_more_than_1000,
            'Distance should be less than 1000'
        )

    def test_is_triplet_angle_above(self):
        """
        Check that the maximum angle check works properly
        by making sure that the angle formed by the three
        points exceeds 1 degree
        """
        triplet: CoordinateTriplet = [
            (0, 0),
            (1, 1),
            (2, 2)
        ]  # angle is 180 degrees

        max_angle = 1
        exceeds_max_angle = is_triplet_angle_above(
            triplet,
            max_angle
        )
        processed_angle = get_angle_from_triplet(triplet)

        self.assertTrue(
            exceeds_max_angle,
            f"{processed_angle} is higher than {max_angle}"
        )

        triplet: CoordinateTriplet = [
            (0, 0),
            (0, 0),
            (0, 0)
        ]  # angle is 180 degrees

        max_angle = 1
        exceeds_max_angle = is_triplet_angle_above(
            triplet,
            max_angle
        )
        processed_angle = get_angle_from_triplet(triplet)

        self.assertFalse(
            exceeds_max_angle,
            f"{processed_angle} is lower than {max_angle}"
        )

    def test_remove_spikes_from_polygon(self):
        """
        Check that the method removes spikes properly.
        The input is a polygon with a known spike, we check
        that the spike was removed by counting the amount
        of vertices in the cleaned polygon.
        """
        polygon_with_one_spike = Polygon(
            properties={},
            coordinates=[
                    [
                        (
                            18.57016026617573,
                            -33.912171286755274
                        ),
                        (
                            18.65947366303845,
                            -33.844666975172984
                        ),
                        (
                            18.76488424189387,
                            -33.85297519813696
                        ),
                        (
                            18.8038290370375,
                            -33.93346110810046
                        ),
                        (
                            18.792405230462034,
                            -34.0004461557475
                        ),
                        (
                            18.73009355823223,
                            -34.02796714431566
                        ),
                        (
                            18.657915871232706,
                            -34.03315978366815
                        ),
                        (
                            18.60858579738411,
                            -34.004600267229485
                        ),
                        (
                            18.5795070170102,
                            -33.963578416344866
                        ),
                        (
                            18.571198794046225,
                            -33.91840245397825
                        ),
                        (
                            18.570244036171502,
                            -33.91230820898571
                        ),
                        (
                            8.293412877884293,
                            -29.78021620019185
                        ),
                        (
                            18.57016026617573,
                            -33.912171286755274
                        )
                    ]
                ]
        )

        max_angle = 1
        min_length = 100
        spike_count = 1
        normalize_coordinates = True
        point_count_before = len(polygon_with_one_spike.coordinates[0])

        cleaned_polygon = remove_spikes_from_polygon(
            polygon_with_one_spike,
            max_angle,
            min_length,
            normalize_coordinates
        )

        point_count_after = len(cleaned_polygon.coordinates[0])

        self.assertEqual(
            point_count_after,
            point_count_before - spike_count,
            "Spikes were not removed properly"
        )
