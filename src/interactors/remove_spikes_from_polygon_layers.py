"""
Use case: remove spikes from a list of polygon layers
"""

from math import atan2, degrees

from interfaces.distance import DistanceProcessor
from models.point import Point
from models.polygon import Polygon, PolygonCoordinates
from models.polygon_layer import PolygonLayer
from models.coordinate_triplet import CoordinateTriplet

from utils import get_triplet_from_list


def is_point_in_geodesic_bounds(point: Point) -> bool:
    """Check if a point falls within the geodesic bounds

    Args:
        point (Point): the point

    Returns:
        bool: wheter the check was passed
    """
    lat, lon = point

    if lat < -90 or lat > 90:
        return False

    if lon < -180 or lon > 180:
        return False

    return True


def get_angle_from_triplet(triplet: CoordinateTriplet) -> float:
    """Returns the value of the angle formed by 3 points

    Args:
        triplet (CoordinateTriplet): the 3 points

    Returns:
        float: angle value in degrees
    """
    point_a, point_b, point_c = triplet

    x_a, y_a = point_a
    x_b, y_b = point_b
    x_c, y_c = point_c

    deg1 = (360 + degrees(atan2(x_a - x_b, y_a - y_b))) % 360
    deg2 = (360 + degrees(atan2(x_c - x_b, y_c - y_b))) % 360

    return deg2 - deg1 if deg1 <= deg2 else 360 - (deg1 - deg2)


def is_distance_to_triplet_middle_point_above(
    triplet: CoordinateTriplet,
    min_length: int
) -> bool:
    """Check if any of the lines that goes to the second
    point in a triplet of points exceeds a set length

    Args:
        triplet (CoordinateTriplet): the 3 points
        min_length (int): the minimum length to check for

    Returns:
        bool: whether the check was passed
    """
    distance_processor = DistanceProcessor()

    point_a, point_b, point_c = triplet

    a_to_b = distance_processor.get_haversine_distance(point_a, point_b)
    b_to_c = distance_processor.get_haversine_distance(point_b, point_c)

    return a_to_b > min_length or b_to_c > min_length


def is_triplet_angle_above(
    triplet: CoordinateTriplet,
    max_angle: int
) -> bool:
    """Checks if the angle formed by a triplet
    of points exceeds a set value.

    For points a, b and c (in this order), the angle
    that will be considered is the one around point b.

    This function considers angles such as 10° and 350°
    to be equal.

    Args:
        triplet (CoordinateTriplet): the 3 points
        max_angle (int): the angle to exceed

    Returns:
        bool: whether the check was passed
    """
    angle = get_angle_from_triplet(triplet)

    return (angle + max_angle) % 360 > max_angle


def is_triplet_middle_point_spike(
    triplet: CoordinateTriplet,
    max_angle: int,
    min_length: int
) -> bool:
    """Call a series of checks that will determine
    if the second point in a triplet of points is
    a spike.

    Args:
        triplet (CoordinateTriplet): the 3 points
        max_angle (int): the angle to exceed
        min_length (int): the minimum length to check for

    Returns:
        bool: whether the check was passed
    """

    exceeds_min_length = is_distance_to_triplet_middle_point_above(
        triplet,
        min_length
    )

    if not exceeds_min_length:
        return False

    exceeds_max_angle = is_triplet_angle_above(
        triplet,
        max_angle
    )

    if exceeds_max_angle:
        return False

    return True


def filter_out_of_geodesic_bounds_points(points: list[Point]) -> list[Point]:
    """Filter out points that are out of the geodesic bounds

    Args:
        points (list[Point]): the list of points

    Returns:
        list[Point]: the filtered list of points
    """
    return [
        point for point in points if is_point_in_geodesic_bounds(point)
    ]


def remove_spikes_from_multipolygon(
    polygon: Polygon,
    max_angle: int,
    min_length: int,
    normalize_coordinates: bool
) -> PolygonCoordinates:
    """Recursively calls the remove_spikes_from_polygon function
    on all the part of a Multipolygon

    Args:
        polygon (Polygon): the multipolygon
        max_angle (int): the angle to exceed
        min_length (int): the minimum length to check for
        normalize_coordinates (bool): should ignore out of bounds coordinates

    Returns:
        PolygonCoordinates: [description]
    """

    parts = []

    for part in polygon.coordinates:
        polygon_from_part = Polygon(part, {})

        cleaned_part = remove_spikes_from_polygon(
            polygon_from_part,
            max_angle,
            min_length,
            normalize_coordinates
        )

        parts.append(cleaned_part.coordinates)

    return polygon._replace(coordinates=parts)


def remove_spikes_from_polygon(
    polygon: Polygon,
    max_angle: int,
    min_length: int,
    normalize_coordinates: bool
) -> Polygon:
    """Return a new polygon with the geometry and properties
    from the passed polygon, but cleaned from its spikes

    Args:
        polygon (Polygon): the polygon
        max_angle (int): the angle to exceed
        min_length (int): the minimum length to check for
        normalize_coordinates (bool): should ignore out of bounds coordinates

    Returns:
        Polygon: the cleaned polygon
    """
    if polygon.is_multipolygon():
        return remove_spikes_from_multipolygon(**locals())

    cleaned_coordinates = []

    for linear_ring in polygon.coordinates:

        coordinate_triplets = get_triplet_from_list(
            filter_out_of_geodesic_bounds_points(
                linear_ring
            ) if normalize_coordinates else linear_ring
        )

        cleaned_part_coordinates = [
            triplet[1] for triplet in coordinate_triplets
            if not is_triplet_middle_point_spike(
                triplet=triplet,
                min_length=min_length,
                max_angle=max_angle
            )
        ]

        cleaned_coordinates.append(cleaned_part_coordinates)

    cleaned_feature = Polygon(
        coordinates=cleaned_coordinates,
        properties=polygon.properties
    )

    return cleaned_feature


def remove_spikes_from_polygon_layer(
    polygon_layer: PolygonLayer,
    max_angle: int,
    min_length: int,
    normalize_coordinates: bool
) -> PolygonLayer:
    """Return a new polygon layer with all the polygons
    from the original layers, but with their spikes
    removed

    Args:
        polygon_layer (PolygonLayer): the layer to clean
        max_angle (int): the angle not to exceed
        min_length (int): the minimum length to check for
        normalize_coordinates (bool): should ignore out of bounds coordinates

    Returns:
        PolygonLayer: [description]
    """
    cleaned_features = [
        remove_spikes_from_polygon(
            polygon,
            max_angle,
            min_length,
            normalize_coordinates
        )
        for polygon in polygon_layer.features
    ]

    return polygon_layer._replace(features=cleaned_features)
