'''
Test the selected GeopackageProcessor set in
src/interfaces/geopackage/__init__.py
'''

from unittest import TestCase
from typing import OrderedDict

from interfaces.geopackage import GeopackageProcessor
from models.polygon import Polygon
from models.polygon_layer import PolygonLayer
from utils import get_project_root


# pylint: disable-next=missing-class-docstring
class TestGeopackageProcessor(TestCase):
    def test_get_polygon_layers_from_geopackage_counts(self):
        """
        Check whether the method loads the data properly
        by counting the amount of layers and features
        """

        data_path = f'{get_project_root()}/data'
        geopackage_file_path = f'{data_path}/spike.gpkg'
        geopackage_processor = GeopackageProcessor()
        polygon_layers = geopackage_processor \
            .get_polygon_layers_from_geopackage(
                geopackage_file_path
            )

        layer_count = len(polygon_layers)
        expected_layer_count = 1

        self.assertEqual(
            layer_count,
            expected_layer_count,
            f"Layer count should {expected_layer_count}"
        )

        first_layer = polygon_layers[0]
        feature_count = len(first_layer.features)
        expected_feature_count = 2

        self.assertEqual(
            feature_count,
            expected_feature_count,
            f"Feature count should {expected_feature_count}"
        )

    def test_write_geopackage_from_polygon_layers(self):
        """
        Check whether the method writes the data properly
        by loading it afterward and counting the layers,
        features and theirs coordinates
        """
        proc = GeopackageProcessor()

        data_path = f'{get_project_root()}/data'
        ouput_path = f'{data_path}/test-output.gpkg'

        polygon_a = Polygon(
            coordinates=[[
                (45.764043, 4.835659),
                (45.754043, 4.836659),
                (45.724043, 4.845659),
                (45.794043, 4.865659),
                (45.764043, 4.835659),
            ]],
            properties={"name": "test"},
        )

        polygon_layers = [
            PolygonLayer(
                name="test",
                features=[polygon_a],
                is_coordinate_system_geographic=True,
                geometry='Polygon',
                crs='GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AXIS["Latitude",NORTH],AXIS["Longitude",EAST],AUTHORITY["EPSG","4326"]]',  # pylint: disable=line-too-long
                properties=OrderedDict({"name": "str"})
            )
        ]

        proc.write_geopackage_from_polygon_layers(
            polygon_layers,
            ouput_path
        )

        reloaded_layers = proc.get_polygon_layers_from_geopackage(
            ouput_path
        )

        layer_count = len(reloaded_layers)
        expected_layer_count = 1

        self.assertEqual(
            layer_count,
            expected_layer_count,
            f"Layer count should be {expected_layer_count}"
        )

        first_layer = reloaded_layers[0]
        feature_count = len(first_layer.features)
        expected_feature_count = 1

        self.assertEqual(
            feature_count,
            expected_feature_count,
            f"Feature count should be {expected_feature_count}"
        )

        first_feature = first_layer.features[0]

        expected_coordinates_count = 4 + 1
        feature_coordinates_count = 0

        for feature_part in first_feature.coordinates:
            feature_coordinates_count += len(feature_part)

        self.assertEqual(
            feature_coordinates_count,
            expected_coordinates_count,
            f"Coordinates count should be {expected_coordinates_count}"
        )
