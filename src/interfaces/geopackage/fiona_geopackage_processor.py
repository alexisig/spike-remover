"""
Geopackage processor using the fiona library
"""
from typing import OrderedDict

import fiona
from fiona.collection import Collection
from fiona import crs

from models.polygon_layer import PolygonLayer
from models.polygon import Polygon

from .base_geopackage_processor import BaseGeopackageProcessor

GEOPACKAGE_SHORT_NAME = 'GPKG'
POLYGON = 'Polygon'
INVALID_GEOMETRY_SKIPPED = 'Polygon skipped. Invalid geometry'
MULTIPOLYGON_NOT_SUPPORTED_SKIPPED = 'Multipolygon skipped. Not supported'
MULTIPOLYGON_NOT_SUPPORTED_ERROR = """
'Multipolygon not supported. Use the --skip-multi option to ignore this error'
"""


class FionaPolygonLayerSchema(dict):
    """
    Interface of a Polygon Layer in fiona
    https://fiona.readthedocs.io/en/latest/fiona.html#fiona.open
    """
    properties: OrderedDict
    geometry: str


class FionaPolygonGeometry(dict):
    """
    Interface of a Geometry in fiona
    https://fiona.readthedocs.io/en/latest/manual.html#d-coordinates-and-geometry-types
    """
    type: str
    coordinates: list


class FionaPolygonRecord(dict):
    """
    Interface of a Record in Fiona
    https://fiona.readthedocs.io/en/latest/manual.html#writing-new-files-from-scratch
    """
    geometry: FionaPolygonGeometry
    properties: OrderedDict


# pylint: disable-next=missing-function-docstring
def is_layer_from_geopackage(layer: Collection) -> bool:
    return layer.meta['driver'] == GEOPACKAGE_SHORT_NAME


# pylint: disable-next=missing-function-docstring
def is_layer_geometry_polygon(layer: Collection) -> bool:
    return layer.schema['geometry'] == POLYGON


def get_layer_polygons(
    layer: Collection
) -> list[Polygon]:
    """Creates a list of Polygon from a fiona Collection.
    Filters out all null records.

    Args:
        layer (Collection): the collection from a geopackage provided by fiona

    Returns:
        list[Polygon]: the "library free" extracted Polygons
    """
    if not layer:
        return []

    polygons = []

    for feature in layer:
        if not feature:
            continue

        if 'geometry' not in feature or feature['geometry'] is None:
            print(INVALID_GEOMETRY_SKIPPED)
            continue

        if 'coordinates' not in feature['geometry']:
            print(INVALID_GEOMETRY_SKIPPED)
            continue

        polygons.append(
            Polygon(
                coordinates=feature['geometry']['coordinates'],
                properties=dict(feature['properties'])
            )
        )

    return polygons


# pylint: disable-next=missing-function-docstring
def get_layer_properties(layer: Collection) -> dict:
    return dict(layer.schema['properties'])


# pylint: disable-next=missing-function-docstring
def get_layer_geometry(layer: Collection) -> str:
    return layer.schema['geometry']


# pylint: disable-next=missing-function-docstring
def get_layer_crs_wkt(layer: Collection) -> str:
    return layer.crs_wkt


# pylint: disable-next=missing-function-docstring
def get_polygon_layer_schema(layer: PolygonLayer) -> FionaPolygonLayerSchema:
    return FionaPolygonLayerSchema(
        properties=OrderedDict(layer.properties),
        geometry=layer.geometry
    )


# pylint: disable-next=missing-function-docstring
def get_record_from_polygon_feature(
    layer: PolygonLayer,
    polygon: Polygon
) -> FionaPolygonRecord:
    return FionaPolygonRecord(
        geometry=FionaPolygonGeometry(
            type=layer.geometry,
            coordinates=polygon.coordinates
        ),
        properties=polygon.properties
    )


class GeopackageProcessor(BaseGeopackageProcessor):
    """
    Implementation of the BaseGeopackageProcessor with the fiona library
    """

    def is_layer_coordinate_system_geographic(self, layer: Collection):
        return layer.crs['init'] == crs.from_epsg(4326)['init']

    def get_polygon_layers_from_geopackage(
        self,
        geopackage_file_path: str,
    ) -> list[PolygonLayer]:
        polygon_layers = []

        for layername in fiona.listlayers(geopackage_file_path):
            with fiona.open(geopackage_file_path, layer=layername) as layer:

                if is_layer_from_geopackage(layer) and \
                        is_layer_geometry_polygon(layer):

                    features = get_layer_polygons(layer)
                    is_geographic = self.is_layer_coordinate_system_geographic(
                        layer
                    )
                    properties = get_layer_properties(layer)
                    geometry = get_layer_geometry(layer)
                    _crs = get_layer_crs_wkt(layer)

                    polygon_layer = PolygonLayer(
                        name=layer.name,
                        features=features,
                        is_coordinate_system_geographic=is_geographic,
                        properties=properties,
                        geometry=geometry,
                        crs=_crs
                    )

                    polygon_layers.append(polygon_layer)

        return polygon_layers

    def write_geopackage_from_polygon_layers(
        self,
        polygon_layers: list[PolygonLayer],
        output_file_path: str
    ):
        for layer in polygon_layers:

            with fiona.open(
                output_file_path,
                'w', driver=GEOPACKAGE_SHORT_NAME,
                layer=layer.name,
                schema=get_polygon_layer_schema(layer),
                crs=layer.crs
            ) as output_geopackage:

                for feature in layer.features:
                    record = get_record_from_polygon_feature(
                        layer=layer,
                        polygon=feature
                    )
                    output_geopackage.write(record)

        return output_file_path
