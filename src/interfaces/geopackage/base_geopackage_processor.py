"""
Base geopackage processor
"""
from abc import abstractmethod
from typing import Any

from models.polygon_layer import PolygonLayer


class BaseGeopackageProcessor:
    """
    Base geopackage processor: defines abstract methods for geopackage
    processors to implement
    """

    @abstractmethod
    def is_layer_coordinate_system_geographic(self, layer: Any) -> bool:
        """Checks whether a layer uses a geographic coordinate system

        Args:
            layer (Any): the layer

        Raises:
            NotImplementedError: raised if the implementation doesn't include
            this method

        Returns:
            bool: whether the check passed
        """
        raise NotImplementedError

    @abstractmethod
    def get_polygon_layers_from_geopackage(
        self,
        geopackage_file_path: str
    ) -> list[PolygonLayer]:
        """Return a list of "library free" PolygonLayer from a geopackage file

        Args:
            geopackage_file_path (str): the path at which to find
            the geopackage file

        Raises:
            NotImplementedError: raised if the implementation doesn't include
            this method

        Returns:
            list[PolygonLayer]: the loaded layers from the geopackage
        """
        raise NotImplementedError

    @abstractmethod
    def write_geopackage_from_polygon_layers(
        self,
        polygon_layers: list[PolygonLayer],
        output_file_path: str
    ) -> str:
        """Take a list of PolygonLayer and write their content
        to a geopackage file

        Args:
            polygon_layers (list[PolygonLayer]): the list of PolygonLayer
            to export
            output_file_path (str): the path of the geopackage to write

        Raises:
            NotImplementedError: raised if the implementation doesn't include
            this method

        Returns:
            str: the path of the created geopackage
        """
        raise NotImplementedError
