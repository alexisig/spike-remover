"""
DistanceProcessor using the haversine library
"""
from haversine import haversine, Unit

from models.point import Point
from .base_distance_processor import BaseDistanceProcessor


class DistanceProcessor(BaseDistanceProcessor):
    """
    Implementation of the BaseDistanceProcessor with the haversine library
    """
    def get_haversine_distance(self, point_a: Point, point_b: Point):
        return haversine(point_a, point_b, unit=Unit.KILOMETERS)
