'''
Test the selected DistanceProcessor set in src/interfaces/distance/__init__.py
'''

from unittest import TestCase

from interfaces.distance import DistanceProcessor
from models.point import Point


# pylint: disable-next=missing-class-docstring
class TestDistanceProcessor(TestCase):
    def test_haversine_distance(self):
        """
        Test that the haversine distance is calculated properly
        based on a known distance.
        """
        lyon_coordinates: Point = (45.764043, 4.835659)
        cape_twon_coordinates: Point = (-33.918861, 18.423300)
        expected_distance = 8930.043996063028
        decimals_to_consider = 2

        distance_processor = DistanceProcessor()
        processed_distance = distance_processor.get_haversine_distance(
            lyon_coordinates,
            cape_twon_coordinates
        )

        self.assertAlmostEqual(
            expected_distance,
            processed_distance,
            decimals_to_consider,
            f"Distance should be {expected_distance}"
        )
