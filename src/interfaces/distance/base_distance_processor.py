"""
Base distance processor
"""
from abc import abstractmethod

from models.point import Point


class BaseDistanceProcessor:  # pylint: disable=too-few-public-methods
    """
    Base distance processor: defines abstract methods for distance
    processors to implement.
    """
    @abstractmethod
    def get_haversine_distance(self, point_a: Point, point_b: Point) -> float:
        """Get the geodesic distance in kilometers between two points

        Args:
            point_a (Point): first point
            point_b (Point): second point

        Returns:
            float: the distance between the two points

        Raises:
            NotImplementedError: raised if called and the implementation
            doesn't include this method
        """
        raise NotImplementedError
