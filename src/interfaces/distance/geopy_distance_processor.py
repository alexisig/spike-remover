"""
DistanceProcessor using the geopy library
"""
from geopy.distance import geodesic

from models.point import Point
from .base_distance_processor import BaseDistanceProcessor


class DistanceProcessor(BaseDistanceProcessor):
    """
    Implementation of the BaseDistanceProcessor with the geopy library
    """
    def get_haversine_distance(self, point_a: Point, point_b: Point):
        return geodesic(point_a, point_b).kilometers
