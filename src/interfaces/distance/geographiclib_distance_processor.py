"""
DistanceProcessor using the geographiclib library
"""
from geographiclib.geodesic import Geodesic

from models.point import Point
from .base_distance_processor import BaseDistanceProcessor


class DistanceProcessor(BaseDistanceProcessor):
    """
    Implementation of the BaseDistanceProcessor with the geographiclib library
    """
    def get_haversine_distance(self, point_a: Point, point_b: Point):
        x_a, y_a = point_a
        x_b, y_b = point_b
        return Geodesic.WGS84.Inverse(x_a, y_a, x_b, y_b)['s12'] / 1000
