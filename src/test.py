"""
Discover and run all tests
"""

from unittest import TestLoader, TextTestRunner
from pathlib import Path

test_loader = TestLoader()
test_runner = TextTestRunner()

test_suite = test_loader.discover(
    start_dir=Path(__file__).resolve().parent,
    pattern='test_*.py'
)

test_runner.run(test_suite)
