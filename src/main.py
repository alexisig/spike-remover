"""
Main entry of the program.

This reads the CLI arguments and execute the removal of the spikes in the
polygon layers of a Geopackage file.

Multipolygons are not supported.
"""

import os

from argparse import ArgumentParser, BooleanOptionalAction

from interfaces.geopackage import GeopackageProcessor
from interactors.remove_spikes_from_polygon_layers import \
    remove_spikes_from_polygon_layer


PARSER_DESCRIPTION = 'Remove spikes from Polygons in a Geopackage'

FILE_PATH_HELP = 'The path of the geopackage file to remove the spikes from'
OUTPUT_PATH_HELP = 'The path where to save the new geopackage'
MAX_ANGLE_HELP = '''
The threshold angle in degrees above which a point in a polygon's linear ring
is not considered a spike
'''
MIN_LENGTH_HELP = '''
The threshold length in kilometers under which the length of the line
toward and from a point in a linear ring is not considered long enough
to be a spike
'''
SKIP_MULLTI_HELP = '''
Ignore erros thrown when trying to process Multipolygons. They will be filtered
out from the output layers
'''
NORMALIZE_COORDINATES_HELP = '''
Whether points with coordinates that falls outside the latitude and
longitude limits should be considered spikes and filtered out
'''

parser = ArgumentParser(description=PARSER_DESCRIPTION)

parser.add_argument(dest='geopackage_file_path', type=str, help=FILE_PATH_HELP)
parser.add_argument(dest='output_file_path', type=str, help=OUTPUT_PATH_HELP)
parser.add_argument('--max-angle', type=int, help=MAX_ANGLE_HELP, default=1)
parser.add_argument('--min-length', help=MIN_LENGTH_HELP, type=int,
                    default=100)
parser.add_argument('--normalize-coordinates', action=BooleanOptionalAction,
                    help=NORMALIZE_COORDINATES_HELP, default=True)

args = parser.parse_args()

geopackage_file_path = args.geopackage_file_path
output_file_path = args.output_file_path
max_angle = args.max_angle
min_length = args.min_length
normalize_coordinates = args.normalize_coordinates

if not os.path.isfile(geopackage_file_path):
    error = f'No geopackage file found at {geopackage_file_path}'
    raise Exception(error)

gp = GeopackageProcessor()

polygon_layers = gp.get_polygon_layers_from_geopackage(
    geopackage_file_path=geopackage_file_path
)

cleaned_polygon_layers = [remove_spikes_from_polygon_layer(
    polygon_layer=polygon_layer,
    max_angle=max_angle,
    min_length=min_length,
    normalize_coordinates=normalize_coordinates
) for polygon_layer in polygon_layers]

gp.write_geopackage_from_polygon_layers(
    polygon_layers=cleaned_polygon_layers,
    output_file_path=output_file_path
)
