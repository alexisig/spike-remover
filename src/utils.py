"""
Various convenience functions
"""

from typing import Any
from pathlib import Path


def get_triplet_from_list(_list: list) -> list[tuple[Any, Any, Any]]:
    """Get triplets of the values before and after each
    element of a list. (before, element after)

    For example, the list [1, 2, 3]
    Will return: [(3, 1, 2), (1, 2, 3), (2, 3, 1)]

    Args:
        _list (list): the list to create triplets from

    Returns:
        list[tuple[Any, Any, Any]]: the list of triplets
    """
    chunks = []

    for index, point in enumerate(_list):
        previous_index = index - 1 if index != 0 else len(_list) - 1
        next_index = index + 1 if index < len(_list) - 1 else 0

        previous_point = _list[previous_index]
        next_point = _list[next_index]

        chunks.append((previous_point, point, next_point))

    return chunks


def get_project_root() -> str:
    """Get the absolute root folder path of the project

    Returns:
        str: the root folder path
    """
    return str(Path(__file__).parent.parent.resolve())


def count_nested_lists(_l: list, count=0) -> int:
    """Recursive function that counts the amount of
    nested list(s) in a list

    Args:
        _l (list): The list to count lists in
        count (int, optional): the count of lists,
        passed recursively. Defaults to 0.

    Returns:
        int: the total count of nested list(s)
    """
    if isinstance(_l, list):
        return count_nested_lists(_l[0], count + 1)
    return count
