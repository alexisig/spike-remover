# Spike remover

CLI tool that removes spikes from Polygons

## Installation

```properties
python3 -m venv env
source env/bin/active
pip install -r requirements.txt
```

## Usage

```
usage: main.py [-h] [--max-angle MAX_ANGLE] [--min-length MIN_LENGTH] [--normalize-coordinates | --no-normalize-coordinates] geopackage_file_path output_file_path

Remove spikes from Polygons in a Geopackage

positional arguments:
  geopackage_file_path  The path of the geopackage file to remove the spikes from
  output_file_path      The path where to save the new geopackage

optional arguments:
  -h, --help            show this help message and exit
  --max-angle MAX_ANGLE
                        The threshold angle in degrees above which a point in a polygon's linear ring is not considered a spike
  --min-length MIN_LENGTH
                        The threshold length in kilometers under which the length of the line toward and from a point in a linear ring is not considered long enough to be a spike
  --normalize-coordinates, --no-normalize-coordinates
                        Whether points with coordinates that falls outside the latitude and longitude limits should be considered spikes and filtered out (default: True)

```

## Example

Using the default options:

```properties
python src/main.py /data/spike.gpkg output.gpkg
```

Using customs options:
```properties
python src/main.py /data/spike.gpkg output.gpkg --max-angle=1 --min-length=100 --skip-multi
```

## Software flow

<img src="diagram.svg"/>

## Software Architecture
 
The architecture applies some of the concepts of the clean architecture, specifically, the independence from frameworks and external libraries, and the fact that the core logic ignores the interface.

Each folder in the `interfaces` folder defines a base processor, 
which is implemented in the files that sit next to it. For example,with the distance processor:
- Base: `src/interfaces/distance/base_distance_processor.py`
- Implementation with the haversine package: `src/interfaces/distance/haversine_distance_processor.py`
- Implementation with the geopy package: `src/interfaces/distance/geopy_distance_processor.py`

This allow the program to be easily upgraded, external libraries easily swapped and overall this allows to create tests that are independent of the external libraries.

The business logic in contained in the `interactors` folder and is completely independant of external logic (aside from the python standard library).

## Known limitations

- Only Polygon layers will be in the ouput geopackage
- The program only handles the wsg84 geodetic system
